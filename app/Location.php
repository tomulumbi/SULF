<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Location extends Model
{
    //
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected static function boot() {
    parent::boot();

    static::deleting(function($location) {
        $location->area()->delete();
    });
    }

    public function area(){
         return $this->hasMany('App\Area', 'locationid', 'id');
      }

}
