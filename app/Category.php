<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    //
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected static function boot() {
    parent::boot();

    static::deleting(function($category) {
        $category->subcategory()->delete();
    });
    }

    public function subcategory(){
         return $this->hasMany('App\SubCategories', 'categoryid', 'id');
      }
}
