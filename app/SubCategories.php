<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubCategories extends Model
{
    //
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected static function boot() {
    parent::boot();

    static::deleting(function($subcategory) {
        $subcategory->brand()->delete();
    });
    }

      public function category(){
         return $this->belongsTo('App\Category' , 'id', 'categoryid');
      }

      public function brand(){
          return $this->hasMany('App\Brand', 'subcategoryid', 'id');
      }
}
