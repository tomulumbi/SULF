<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use App\Administrator;
use App\Claim;

class admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        // if ($request->age <= 200) {
        //     return redirect('home');
        // }

        $value = Session::get('suid');
				$user = Administrator::where('suid', $value)->first();

				if ($user == false) {
					$claim = Claim::where('status','=','34')->where('viewed','=','6')->count();
					$pending = Claim::where('status','=','34')->where('claimersuid',"=",$value)->count();
					$rejected = Claim::where('status','=','55')->where('claimersuid',"=",$value)->count();
					$reclaimed = Claim::where('status','=','99')->where('claimersuid',"=",$value)->count();
				return redirect('dash')->withValue($value)->withPending($pending)->withRejected($rejected)->withReclaimed($reclaimed)->withClaim($claim);
			}


        return $next($request);
    }
}
