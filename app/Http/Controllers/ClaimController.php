<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Item;
use Illuminate\Support\Facades\DB;
use App\Administrator;
use App\Claim;

class ClaimController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if(Session::has('suid')) {
        $value = Session::get('suid');
        return view('UserDash.claimform')->withValue($value);
      } else {
        return redirect('userlogin');
      }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //
        $value = Session::get('suid');
        $studentid = DB::table('students')->where('suid', '=', $value)->value('id');
        // dd($request);
        $this->validate($request, array(
                'category' => 'required|max:255',
                'subcategory' => 'required|max:255',
                'brand' => 'required|max:255',
                'model' => 'max:255',
                'serialnumber' => 'max:255',
                'primarycolor' => 'required|max:255',
                'secondarycolor' => 'required|max:255',
                'description' => 'required|max:255',
                'claimersuid' => 'required|max:255',
                'claimeremail' => 'required|email|max:255',
                'claimerphonenumber' => 'required|max:255',
                'location' => 'required|max:255',
                'area' => 'required|max:255',
                'datelost' => 'required|max:255',
            ));
            $claim = new Claim;
            $claim->brand = ($request->brand);
            $claim->subcategory = ($request->subcategory);
            $claim->primarycolorid = decrypt($request->primarycolor);
            $claim->secondarycolorid = decrypt($request->secondarycolor);
            $claim->areaid = ($request->area);
            $claim->studentid = $value;
            $claim->model = $request->model;
            $claim->serialnumber = $request->serialnumber;
            $claim->description = $request->description;
            $claim->claimersuid = $request->claimersuid;
            $claim->claimeremail = $request->claimeremail;
            $claim->claimerphonenumber = $request->claimerphonenumber;
            $claim->status = 34;
            $claim->viewed = 6;
            $claim->save();

            Session::flash('userregistered', 'Item has been registered as lost!');
            return redirect()->route('pending.index')->withValue($value)->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
