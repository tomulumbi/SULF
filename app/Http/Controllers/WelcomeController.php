<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Student;
use Session;
use App\Http\Controllers\Input;
use App\ContributedItems;
use App\ClaimedItems;
use App\Claim;
use App\Administrator;
use App\Brand;
use App\Area;
use App\SubCategories;
use App\Item;
use Cas;
/**
*
*/
class WelcomeController extends Controller
{

		public function getIndex()
		{

			// if (Auth::check()) {
			// 	$item = Item::where('status','=','34')->count();
			// 	$claim = Claim::where('status','=','34')->where('viewed','=','6')->count();
			// 	$pending = Claim::where('status','=','34')->count();
			// 	$contributed = ContributedItems::where('status','=','34')->count();
			// 	$rejected = Claim::where('status','=','55')->count();
			// 	$reclaimed = Claim::where('status','=','99')->count();
			// 	return view('AdminDash.dashboard')->withItem($item)->withPending($pending)->withRejected($rejected)->withReclaimed($reclaimed)->withContributed($contributed)->withClaim($claim);
			// }
			if (Session::has('suid')) {
				$value = Session::get('suid');
				$user = Administrator::where('suid', $value)->first();

				if ($user == false) {
					return view('Welcome.welcome');
			}elseif ($user == true) {

						$item = Item::where('status','=','34')->count();
						$claim = Claim::where('status','=','34')->where('viewed','=','6')->count();
						$pending = Claim::where('status','=','34')->count();
						$contributed = ContributedItems::where('status','=','34')->count();
						$rejected = Claim::where('status','=','55')->count();
						$reclaimed = Claim::where('status','=','99')->count();
						return view('AdminDash.dashboard')->withValue($value)->withItem($item)->withPending($pending)->withRejected($rejected)->withReclaimed($reclaimed)->withContributed($contributed)->withClaim($claim);
				}else {
					return view('Welcome.welcome');
				}

		}
		else {
			return view('Welcome.welcome');
		}
	}
		public function getUserLogin()
		{
			return redirect('/');
		}
		public function postStudentLogin(Request $request)
		{
			$this->validate($request, array(
							'suid' => 'required|max:255',
					));
					if (Student::where('suid', '=', $request->suid)->exists()) {
					   // user found
						 Session::set('suid', $request->suid);
						 return redirect('dash');
					}else {
						return view('Welcome.userlogin');
					}
		}
		public function getAdminLogin()
		{
			return view('Welcome.adminlogin');
		}
}
?>
