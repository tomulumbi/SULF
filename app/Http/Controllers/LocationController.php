<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Administrator;
use App\Location;
use App\Claim;
use Session;
use SoftDeletes;

class LocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $value = Session::get('suid');
        $claim = Claim::where('status','=','34')->where('viewed','=','6')->count();
        return view('AdminDash.locations')->withValue($value)->withClaim($claim);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $value = Session::get('suid');
        $claim = Claim::where('status','=','34')->where('viewed','=','6')->count();
        return view('AdminDash.locationregistration')->withValue($value)->withClaim($claim);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $value = Session::get('suid');
        $id = Administrator::where('suid', $value)->first()->id;
        $this->validate($request, array(
                'location' => 'required|max:255',
            ));
          $location = new Location;
          $location->location = $request->location;
          $location->status = '34';
          $location->userid = $id;
          $location->save();

          Session::flash('registered', 'Location has been registered!');
          return redirect()->route('location.index')->withValue($value);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $value = Session::get('suid');
        $this->validate($request, array(
                'location' => 'required|max:255',
            ));
        $location = Location::find(decrypt($id));
        $location->location = $request->location;
        $location->save();
        Session::flash('registered', 'Location has been updated!');
        return redirect()->route('location.index')->withValue($value);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $value = Session::get('suid');
        $location = Location::find(decrypt($id));
        $location->delete();
        Session::flash('registered', 'Location has been deleted!');
        return redirect()->route('location.index')->withValue($value);
    }
}
