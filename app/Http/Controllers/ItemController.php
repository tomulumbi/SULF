<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Administrator;
use App\Location;
use App\Area;
use App\Item;
use App\Claim;
use Session;
use SoftDeletes;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $value = Session::get('suid');
        $claim = Claim::where('status','=','34')->where('viewed','=','6')->count();
        return view('AdminDash.items')->withValue($value)->withClaim($claim);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $value = Session::get('suid');
        $claim = Claim::where('status','=','34')->where('viewed','=','6')->count();
        return view('AdminDash.itemregistration')->withValue($value)->withClaim($claim);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // return dd($request);
        $value = Session::get('suid');
        $id = Administrator::where('suid', $value)->first()->id;
        $this->validate($request, array(
                'category' => 'required|max:255',
                'subcategory' => 'required|max:255',
                'brand' => 'max:255',
                'model' => 'max:255',
                'serialnumber' => 'max:255',
                'primarycolor' => 'required|max:255',
                'secondarycolor' => 'required|max:255',
                'description' => 'required|max:255',
                'findersuid' => 'required|max:255',
                'finderphonenumber' => 'required|max:255',
                'location' => 'required|max:255',
                'area' => 'required|max:255',
                'datelost' => 'required|max:255',
            ));
            $items = new Item;
            $items->brand = ($request->brand);
            $items->subcategory = ($request->subcategory);
            $items->primarycolorid = decrypt($request->primarycolor);
            $items->secondarycolorid = decrypt($request->secondarycolor);
            $items->areaid = ($request->area);
            $items->userid = $id;
            $items->datelost = $request->datelost;
            $items->model = $request->model;
            $items->serialnumber = $request->serialnumber;
            $items->description = $request->description;
            $items->findersuid = $request->findersuid;
            $items->finderemail = 'email@gmail.com';
            $items->finderphonenumber = $request->finderphonenumber;
            $items->status = 34;
            $items->save();
            Session::flash('userregistered', 'Item has been registered as lost!');
            return redirect()->route('item.index')->withValue($value)->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $value = Session::get('suid');
        $val = decrypt($id);
        $claim = Claim::where('status','=','34')->where('viewed','=','6')->count();
        return view('AdminDash.compare')->withValue($value)->withVal($val)->withClaim($claim);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $value = decrypt($id);
        DB::table('items')
            ->where('id', $value)
            ->update(['votes' => 1]);
            DB::table('claims')
                ->where('id', $value)
                ->update(['votes' => 1]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $value = Session::get('suid');
        // dd($request);
        $this->validate($request, array(
          'subcategory' => 'required|max:255',
          'brand' => 'max:255',
          'model' => 'max:255',
          'serialnumber' => 'max:255',
          'primarycolor' => 'required|max:255',
          'secondarycolor' => 'required|max:255',
          'description' => 'required|max:255',
          'findersuid' => 'required|max:255',
          'finderphonenumber' => 'required|max:255',
          'location' => 'required|max:255',
          'area' => 'required|max:255',
          'datelost' => 'required|max:255',
            ));
            $items = Item::find(decrypt($id));
            $items->brand = ($request->brand);
            $items->subcategory = ($request->subcategory);
            $items->primarycolorid = ($request->primarycolor);
            $items->secondarycolorid = ($request->secondarycolor);
            $items->areaid = ($request->area);
            $items->userid = Administrator::where('suid', $value)->first()->id;
            $items->datelost = $request->datelost;
            $items->model = $request->model;
            $items->serialnumber = $request->serialnumber;
            $items->description = $request->description;
            $items->findersuid = $request->findersuid;
            $items->finderemail = 'email@gmail.com';
            $items->finderphonenumber = $request->finderphonenumber;
            $items->save();
            Session::flash('userregistered', 'Item has been updatedt!');
            return redirect()->route('item.index')->withValue($value)->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
