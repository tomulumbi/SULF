<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Administrator;
use App\Category;
use App\Claim;
use App\SubCategories;
use Session;
use SoftDeletes;

class SubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $value = Session::get('suid');
        $claim = Claim::where('status','=','34')->where('viewed','=','6')->count();
        return view('AdminDash.subcategories')->withValue($value)->withClaim($claim);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $value = Session::get('suid');
        $claim = Claim::where('status','=','34')->where('viewed','=','6')->count();
        return view('AdminDash.subcategoryregistration')->withValue($value)->withClaim($claim);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $value = Session::get('suid');
        $id = Administrator::where('suid', $value)->first()->id;
        $this->validate($request, array(
                'category' => 'required|max:255',
                'subcategory' => 'required|max:255',
            ));
          $category = new SubCategories;
          $category->categoryid = decrypt($request->category);
          $category->subcategory = $request->subcategory;
          $category->status = '34';
          $category->userid = $id;
          $category->save();

          Session::flash('registered', 'Sub-Category has been registered!');
          return redirect()->route('subcategory.index')->withValue($value);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        {
            //
            $value = Session::get('suid');
            $this->validate($request, array(
                    'category' => 'required|max:255',
                    'subcategory' => 'required|max:255',
                ));
            $category = SubCategories::find(decrypt($id));
            $category->categoryid = decrypt($request->category);
            $category->subcategory = $request->subcategory;
            $category->save();
            Session::flash('registered', 'Sub Category has been Updated!');
            return redirect()->route('subcategory.index')->withValue($value);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $value = Session::get('suid');
        $category = SubCategories::find(decrypt($id));
        $category->delete();
        Session::flash('registered', 'Sub Category has been deleted!');
        return redirect()->route('subcategory.index')->withValue($value);
    }
}
