<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Administrator;
use App\Color;
use App\Claim;
use Session;
use SoftDeletes;

class ColorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $value = Session::get('suid');
        $claim = Claim::where('status','=','34')->where('viewed','=','6')->count();
        return view('AdminDash.colors')->withValue($value)->withClaim($claim);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $value = Session::get('suid');
        $claim = Claim::where('status','=','34')->where('viewed','=','6')->count();
        return view('AdminDash.colorregistration')->withValue($value)->withClaim($claim);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $value = Session::get('suid');
        $id = Administrator::where('suid', $value)->first()->id;
        $this->validate($request, array(
                'color' => 'required|max:255',
            ));
          $color = new Color;
          $color->color = $request->color;
          $color->status = '34';
          $color->userid = $id;
          $color->save();

          Session::flash('registered', 'Color has been registered!');
          return redirect()->route('color.index')->withValue($value);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $value = Session::get('suid');
        $this->validate($request, array(
                'color' => 'required|max:255',
            ));
        $color = Color::find(decrypt($id));
        $color->color = $request->color;
        $color->save();
        Session::flash('registered', 'Color has been updated!');
        return redirect()->route('color.index')->withValue($value);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $value = Session::get('suid');
        $color = Color::find(decrypt($id));
        $color->delete();
        Session::flash('registered', 'Color has been deleted!');
        return redirect()->route('color.index')->withValue($value);
    }
}
