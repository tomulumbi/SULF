<?php

namespace App\Http\Controllers;
use Session;
use App\Item;
use App\Administrator;
use App\ContributedItems;
use App\Claim;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $value = Session::get('suid');
      $item = Item::where('status','=','34')->count();
			$pending = Claim::where('status','=','34')->count();
			$contributed = ContributedItems::where('status','=','34')->count();
			$rejected = Claim::where('status','=','55')->count();
			$reclaimed = Claim::where('status','=','99')->count();
      $claim = Claim::where('status','=','34')->where('viewed','=','6')->count();
        return view('home')->withValue($value)->withItem($item)->withPending($pending)->withRejected($rejected)->withReclaimed($reclaimed)->withContributed($contributed)->withClaim($claim);
    }
}
