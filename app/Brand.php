<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Brand extends Model
{
    //
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    
    public function subcategory(){
       return $this->hasOne('App\SubCategories');
    }
}
