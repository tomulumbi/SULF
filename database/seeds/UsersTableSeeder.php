<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert([
            'name' => 'romoka',
            'email' => 'romoka@strathmore.edu',
            'password' => bcrypt('SU1F4DM!N!STR4T0R'),
            'created_at' => Carbon::now(),
        ]);
        DB::table('administrators')->insert([
            'userid' => 1,
            'registererid' => 1,
            'suid' => 'romoka',
            'username' => 'romoka',
            'status' => 0,
            'created_at' => Carbon::now(),
        ]);
        DB::table('students')->insert([
            'suid' => '78376',
            'created_at' => Carbon::now(),
        ]);
        DB::table('students')->insert([
            'suid' => 'romoka',
            'created_at' => Carbon::now(),
        ]);
    }
}
