<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class BrandTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $data = [
            ['brand' => 'Samsung','status' => '34', 'subcategoryid' => '1', 'userid' => '1', 'created_at' => Carbon::now()],
            ['brand' => 'Apple','status' => '34', 'subcategoryid' => '1', 'userid' => '1', 'created_at' => Carbon::now()],
            ['brand' => 'Microsoft','status' => '34', 'subcategoryid' => '1', 'userid' => '1', 'created_at' => Carbon::now()],
            ['brand' => 'Nokia','status' => '34', 'subcategoryid' => '1', 'userid' => '1', 'created_at' => Carbon::now()],
            ['brand' => 'Sony','status' => '34', 'subcategoryid' => '1', 'userid' => '1', 'created_at' => Carbon::now()],
            ['brand' => 'LG','status' => '34', 'subcategoryid' => '1', 'userid' => '1', 'created_at' => Carbon::now()],
            ['brand' => 'HTC','status' => '34', 'subcategoryid' => '1', 'userid' => '1', 'created_at' => Carbon::now()],
            ['brand' => 'Motorola','status' => '34', 'subcategoryid' => '1', 'userid' => '1', 'created_at' => Carbon::now()],
            ['brand' => 'Infinix','status' => '34', 'subcategoryid' => '1', 'userid' => '1', 'created_at' => Carbon::now()],
            ['brand' => 'Tecno','status' => '34', 'subcategoryid' => '1', 'userid' => '1', 'created_at' => Carbon::now()],
            ['brand' => 'Alcatel','status' => '34', 'subcategoryid' => '1', 'userid' => '1', 'created_at' => Carbon::now()],
            ['brand' => 'Lenovo','status' => '34', 'subcategoryid' => '1', 'userid' => '1', 'created_at' => Carbon::now()],
            ['brand' => 'Huawei','status' => '34', 'subcategoryid' => '1', 'userid' => '1', 'created_at' => Carbon::now()],
            ['brand' => 'XIAOMI','status' => '34', 'subcategoryid' => '1', 'userid' => '1', 'created_at' => Carbon::now()],
            ['brand' => 'Google','status' => '34', 'subcategoryid' => '1', 'userid' => '1', 'created_at' => Carbon::now()],
            ['brand' => 'Toshiba','status' => '34', 'subcategoryid' => '1', 'userid' => '1', 'created_at' => Carbon::now()],
            ['brand' => 'HP','status' => '34', 'subcategoryid' => '1', 'userid' => '1', 'created_at' => Carbon::now()],
            ['brand' => 'Other','status' => '34', 'subcategoryid' => '1', 'userid' => '1', 'created_at' => Carbon::now()],

        ];

        DB::table('brands')->insert($data);
    }
}
