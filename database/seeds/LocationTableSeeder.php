<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class LocationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
      $data = [
        ['location' => 'Phase 1','status' => '34', 'userid' => '1', 'created_at' => Carbon::now()],
        ['location' => 'Strathmore Business School','status' => '34', 'userid' => '1', 'created_at' => Carbon::now()],
        ['location' => 'Auditorium','status' => '34', 'userid' => '1', 'created_at' => Carbon::now()],
        ['location' => 'MSB','status' => '34', 'userid' => '1', 'created_at' => Carbon::now()],
        ['location' => 'LSB','status' => '34', 'userid' => '1', 'created_at' => Carbon::now()],
        ['location' => 'Law School','status' => '34', 'userid' => '1', 'created_at' => Carbon::now()],
        //['location' => 'Parking Lot','status' => '34', 'userid' => '1', 'created_at' => Carbon::now()],
        ['location' => 'Graduation Square','status' => '34', 'userid' => '1', 'created_at' => Carbon::now()],
        ['location' => 'Students Center','status' => '34', 'userid' => '1', 'created_at' => Carbon::now()],
      ];
        DB::table('locations')->insert($data);
    }
}
