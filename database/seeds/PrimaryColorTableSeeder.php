<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class PrimaryColorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      $data = [

        ['color' => 'Blue','status' => '34', 'userid' => '1', 'created_at' => Carbon::now()],
        ['color' => 'Green','status' => '34', 'userid' => '1', 'created_at' => Carbon::now()],
        ['color' => 'Red','status' => '34', 'userid' => '1', 'created_at' => Carbon::now()],
        ['color' => 'Black','status' => '34', 'userid' => '1', 'created_at' => Carbon::now()],
        ['color' => 'White','status' => '34', 'userid' => '1', 'created_at' => Carbon::now()],
        ['color' => 'Grey','status' => '34', 'userid' => '1', 'created_at' => Carbon::now()],
        ['color' => 'Yellow','status' => '34', 'userid' => '1', 'created_at' => Carbon::now()],
        ['color' => 'Orange','status' => '34', 'userid' => '1', 'created_at' => Carbon::now()],
        ['color' => 'Pink','status' => '34', 'userid' => '1', 'created_at' => Carbon::now()],
        ['color' => 'Purple','status' => '34', 'userid' => '1', 'created_at' => Carbon::now()],
        ['color' => 'Brown','status' => '34', 'userid' => '1', 'created_at' => Carbon::now()],
        ['color' => 'Violet','status' => '34', 'userid' => '1', 'created_at' => Carbon::now()],
        ['color' => 'Magenta','status' => '34', 'userid' => '1', 'created_at' => Carbon::now()],
        ['color' => 'Gold','status' => '34', 'userid' => '1', 'created_at' => Carbon::now()],
        ['color' => 'Cyan','status' => '34', 'userid' => '1', 'created_at' => Carbon::now()],
        ['color' => 'Indigo','status' => '34', 'userid' => '1', 'created_at' => Carbon::now()],
        ['color' => 'Tarquoise','status' => '34', 'userid' => '1', 'created_at' => Carbon::now()],
        ['color' => 'Maroon','status' => '34', 'userid' => '1', 'created_at' => Carbon::now()],
        ['color' => 'Silver','status' => '34', 'userid' => '1', 'created_at' => Carbon::now()],
        ['color' => 'Other','status' => '34', 'userid' => '1', 'created_at' => Carbon::now()],

      ];
        DB::table('colors')->insert($data);
    }
}
