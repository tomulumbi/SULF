<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(SubCategoriesTableSeeder::class);
        // $this->call(BrandTableSeeder::class);
        $this->call(PrimaryColorTableSeeder::class);
        $this->call(LocationTableSeeder::class);
        $this->call(AreaTableSeeder::class);
    }
}
