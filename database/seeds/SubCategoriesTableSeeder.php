<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class SubCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

         $data = [


             ['subcategory' => 'Phone','status' => '34', 'categoryid' => '6', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Calculator','status' => '34', 'categoryid' => '6', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Camera','status' => '34', 'categoryid' => '6', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Camera Accessory','status' => '34', 'categoryid' => '6', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Laptop','status' => '34', 'categoryid' => '6', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Laptop Accessory','status' => '34', 'categoryid' => '6', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'USB Drive','status' => '34', 'categoryid' => '6', 'userid' => '1', 'created_at' => Carbon::now()],



             ['subcategory' => 'Belt','status' => '34', 'categoryid' => '1', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Glove','status' => '34', 'categoryid' => '1', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Hat','status' => '34', 'categoryid' => '1', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Make-Up','status' => '34', 'categoryid' => '1', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Scarf','status' => '34', 'categoryid' => '1', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Tie','status' => '34', 'categoryid' => '1', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Umbrella','status' => '34', 'categoryid' => '1', 'userid' => '1', 'created_at' => Carbon::now()],



             ['subcategory' => 'Note Book','status' => '34', 'categoryid' => '2', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Address Book','status' => '34', 'categoryid' => '2', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Diary','status' => '34', 'categoryid' => '2', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Hard Cover','status' => '34', 'categoryid' => '2', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Paperback','status' => '34', 'categoryid' => '2', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Soft Cover','status' => '34', 'categoryid' => '2', 'userid' => '1', 'created_at' => Carbon::now()],


             ['subcategory' => 'Backpack','status' => '34', 'categoryid' => '3', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Briefcase','status' => '34', 'categoryid' => '3', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Duffle Bag','status' => '34', 'categoryid' => '3', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Suit Bag','status' => '34', 'categoryid' => '3', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Handbag','status' => '34', 'categoryid' => '3', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Messenger Bag','status' => '34', 'categoryid' => '3', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Shopping Bag','status' => '34', 'categoryid' => '3', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Shoulder Bag','status' => '34', 'categoryid' => '3', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Suitcase','status' => '34', 'categoryid' => '3', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Tote Bag','status' => '34', 'categoryid' => '3', 'userid' => '1', 'created_at' => Carbon::now()],



             ['subcategory' => 'Blouse','status' => '34', 'categoryid' => '4', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Coat','status' => '34', 'categoryid' => '4', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Dress','status' => '34', 'categoryid' => '4', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Jacket','status' => '34', 'categoryid' => '4', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Leather Jacket','status' => '34', 'categoryid' => '4', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Nightwear','status' => '34', 'categoryid' => '4', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Pants/Trouser/shorts','status' => '34', 'categoryid' => '4', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Shirt','status' => '34', 'categoryid' => '4', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Skirt','status' => '34', 'categoryid' => '4', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Suit','status' => '34', 'categoryid' => '4', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Sweater','status' => '34', 'categoryid' => '4', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'T-Shirt','status' => '34', 'categoryid' => '4', 'userid' => '1', 'created_at' => Carbon::now()],



             ['subcategory' => 'Cash','status' => '34', 'categoryid' => '5', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Gift Card','status' => '34', 'categoryid' => '5', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Money Order','status' => '34', 'categoryid' => '5', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Other Currency','status' => '34', 'categoryid' => '5', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Payroll Check','status' => '34', 'categoryid' => '5', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Personal Check','status' => '34', 'categoryid' => '5', 'userid' => '1', 'created_at' => Carbon::now()],



             ['subcategory' => 'Eye Glasses','status' => '34', 'categoryid' => '7', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Sunglasses','status' => '34', 'categoryid' => '7', 'userid' => '1', 'created_at' => Carbon::now()],



             ['subcategory' => 'Boots','status' => '34', 'categoryid' => '8', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Galoshes','status' => '34', 'categoryid' => '8', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Sandals','status' => '34', 'categoryid' => '8', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Shoes','status' => '34', 'categoryid' => '8', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Slippers','status' => '34', 'categoryid' => '8', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Sneakers','status' => '34', 'categoryid' => '8', 'userid' => '1', 'created_at' => Carbon::now()],



             ['subcategory' => 'Birth Certificate','status' => '34', 'categoryid' => '9', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Credit Card','status' => '34', 'categoryid' => '9', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Debit Card','status' => '34', 'categoryid' => '9', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Drivers License','status' => '34', 'categoryid' => '9', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Identification Card','status' => '34', 'categoryid' => '9', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Insuarance Card','status' => '34', 'categoryid' => '9', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Library Card','status' => '34', 'categoryid' => '9', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Membership Card','status' => '34', 'categoryid' => '9', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'School ID','status' => '34', 'categoryid' => '9', 'userid' => '1', 'created_at' => Carbon::now()],



             ['subcategory' => 'Bracelet','status' => '34', 'categoryid' => '10', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Cuff Links','status' => '34', 'categoryid' => '10', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Earings','status' => '34', 'categoryid' => '10', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Fashion','status' => '34', 'categoryid' => '10', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Necklace','status' => '34', 'categoryid' => '10', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Ring','status' => '34', 'categoryid' => '10', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Tie Clips','status' => '34', 'categoryid' => '10', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Watch','status' => '34', 'categoryid' => '10', 'userid' => '1', 'created_at' => Carbon::now()],


             ['subcategory' => 'Car','status' => '34', 'categoryid' => '11', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'House','status' => '34', 'categoryid' => '11', 'userid' => '1', 'created_at' => Carbon::now()],



             ['subcategory' => 'Cane','status' => '34', 'categoryid' => '12', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Inhaler','status' => '34', 'categoryid' => '12', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Medication','status' => '34', 'categoryid' => '12', 'userid' => '1', 'created_at' => Carbon::now()],



             ['subcategory' => 'Art & Crafts','status' => '34', 'categoryid' => '13', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Checkbook','status' => '34', 'categoryid' => '13', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Blueprint','status' => '34', 'categoryid' => '13', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Envelope','status' => '34', 'categoryid' => '13', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Folder','status' => '34', 'categoryid' => '13', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Lunch Box','status' => '34', 'categoryid' => '13', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Mail','status' => '34', 'categoryid' => '13', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Photo/Picture','status' => '34', 'categoryid' => '13', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Portfolio','status' => '34', 'categoryid' => '13', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Stationery','status' => '34', 'categoryid' => '13', 'userid' => '1', 'created_at' => Carbon::now()],



             ['subcategory' => 'Clarinet','status' => '34', 'categoryid' => '14', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Flute','status' => '34', 'categoryid' => '14', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Guitar','status' => '34', 'categoryid' => '14', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Harmonica','status' => '34', 'categoryid' => '14', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Musical Instrument Accessory','status' => '34', 'categoryid' => '14', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Saxophone','status' => '34', 'categoryid' => '14', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Trumpet','status' => '34', 'categoryid' => '14', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Violin','status' => '34', 'categoryid' => '14', 'userid' => '1', 'created_at' => Carbon::now()],



             ['subcategory' => 'Ball','status' => '34', 'categoryid' => '15', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Bat','status' => '34', 'categoryid' => '15', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Bicycle','status' => '34', 'categoryid' => '15', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Binocular','status' => '34', 'categoryid' => '15', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Hockey Stick','status' => '34', 'categoryid' => '15', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Protective Gear','status' => '34', 'categoryid' => '15', 'userid' => '1', 'created_at' => Carbon::now()],



             ['subcategory' => 'Card Holder','status' => '34', 'categoryid' => '16', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'ID Holder','status' => '34', 'categoryid' => '16', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Pouch','status' => '34', 'categoryid' => '16', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Purse','status' => '34', 'categoryid' => '16', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Wallet','status' => '34', 'categoryid' => '16', 'userid' => '1', 'created_at' => Carbon::now()],
             ['subcategory' => 'Wristlet','status' => '34', 'categoryid' => '16', 'userid' => '1', 'created_at' => Carbon::now()],


         ];

         DB::table('sub_categories')->insert($data);
    }
}
