@extends('AdminDash.main')

@section('title')
Dashboard
@endsection()

@section('content')
      <!-- page content -->
      <div class="right_col" role="main">

        <!-- top tiles -->
        <div class="row tile_count">
          <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count">
            <div class="left"></div>
            <div class="right">
              <span class="count_top"><i class="fa fa-user"></i> Total Items</span>
              <div class="count">{{$item}}</div>
              <!-- <span class="count_bottom"><i class="green">4% </i> From last Week</span> -->
            </div>
          </div>
          <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count">
            <div class="left"></div>
            <div class="right">
              <span class="count_top"><i class="fa fa-clock-o"></i> Total Pending Claims</span>
              <div class="count">{{$pending}}</div>
              <!-- <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>3% </i> From last Week</span> -->
            </div>
          </div>
          <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count">
            <div class="left"></div>
            <div class="right">
              <span class="count_top"><i class="fa fa-user"></i> Total Rejected Claimes</span>
              <div class="count green">{{$rejected}}</div>
              <!-- <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>34% </i> From last Week</span> -->
            </div>
          </div>
          <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count">
            <div class="left"></div>
            <div class="right">
              <span class="count_top"><i class="fa fa-user"></i> Total Reclaimed Claims</span>
              <div class="count">{{$reclaimed}}</div>
              <!-- <span class="count_bottom"><i class="red"><i class="fa fa-sort-desc"></i>12% </i> From last Week</span> -->
            </div>
          </div>
          <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count">
            <div class="left"></div>
            <div class="right">
              <span class="count_top"><i class="fa fa-user"></i> Total Contributed Items</span>
              <div class="count">{{$contributed}}</div>
              <!-- <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>34% </i> From last Week</span> -->
            </div>
          </div>

        </div>
        <!-- /top tiles -->
        <!-- Content -->
        <div class="title_left">
          <h3 class="text-center">
                Registered Items
            </h3>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="x_panel">
                    <div class="x_title">
                      <h2> </h2>
                      <ul class="nav navbar-right panel_toolbox">
                        <li><a href="#"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                          <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a>
                            </li>
                            <li><a href="#">Settings 2</a>
                            </li>
                          </ul>
                        </li>
                        <li><a href="#"><i class="fa fa-close"></i></a>
                        </li>
                      </ul>
                      <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                      <p class="text-muted font-13 m-b-30">

                      </p>
                      <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                        <thead>
                          <tr>
                            <th>Serial Number</th>
                            <th>Brand</th>
                            <th>Model</th>
                            <th>Primary Color</th>
                            <th>Secondary Color</th>
                            <th>Location</th>
                            <th>Area</th>
                            <th>Registered</th>
                            <th>Registerer</th>
                            <th>Finder</th>
                            <th>Compare</th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach (\App\Item::where('status', 34)->get(); as $items)
                          <tr>
                            <td>{{$items->serialnumber}}</td>
                            <td>
                              {{
                                $userval = DB::table('brands')->where('id', $items->brandid)->value('brand')
                              }}
                            </td>
                              <td>{{$items->model}}</td>
                            <td>
                              {{
                                $userval = DB::table('colors')->where('id', $items->primarycolorid)->value('color')
                              }}
                            </td>
                            <td>
                              {{
                                $userval = DB::table('colors')->where('id', $items->secondarycolorid)->value('color')
                              }}
                            </td>
                            <td>
                                {{
                                  $userval = DB::table('locations')
                                  ->join('areas', 'locations.id', '=', 'areas.locationid')
                                  ->value('locations.location')
                                }}
                            </td>
                            <td>
                                {{
                                  $userval = DB::table('areas')->where('id', $items->areaid)->value('area')
                                }}
                            </td>
                            <td>{{$items->created_at}}</td>
                            <td>
                              {{
                                $userval = DB::table('users')->where('id', $items->userid)->value('name')
                              }}</td>

                            <!-- Accept Modal -->
                <div class="modal fade" id="{{$items->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel">Accept Modal</h4>
                      </div>
                      <div class="modal-body">
                        <form role="form">
                          <div class="box-body">
                            <div class="form-group">
                              <label for="exampleInputPassword1">Finder SUID</label>
                              <input type="text" readonly="readonly" value="{{$items->findersuid}}" class="form-control" id="_administrator" placeholder="">
                            </div>
                            <div class="form-group">
                              <label for="exampleInputPassword1">Finder Email</label>
                              <input type="text" readonly="readonly" value="{{$items->finderemail}}" class="form-control" id="_administrator" placeholder="">
                            </div>
                            <div class="form-group">
                              <label for="exampleInputPassword1">Finder Phone Number</label>
                              <input type="text" readonly="readonly" value="{{$items->finderphonenumber}}" class="form-control" id="_administrator" placeholder="">
                            </div>
                            <div class="form-group">
                              <label for="exampleInputEmail1">Reason</label>
                              <textarea class="form-control" id="_reason" readonly="readonly" rows="3" placeholder="{{$items->description}}"></textarea>
                            </div>

                          </div>
                          <!-- /.box-body -->

                          <div class="box-footer">
                            <button type="submit" class="btn btn-block btn-success">Submit</button>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
                <td><span class="btn btn-success btn-block" data-toggle="modal" data-target="#{{$items->id}}">More</span></td>
                <td><a href="item/{{encrypt($items->id)}}" class="btn btn-success btn-block">Compare</a></td>
                          </tr>
                          @endforeach()
                        </tbody>
                      </table>

                    </div>
                  </div>
                </div>

      </div>
        <br />


        <!-- footer content -->

        @include('AdminDash.partials._footnote')
        <!-- /footer content -->
      </div>
      <!-- /page content -->

    @endsection()
