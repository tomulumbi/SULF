@extends('Welcome.main')
@section('content')
<nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand page-scroll" href="#page-top">SULF</a>
            </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a class="page-scroll" href="/">Back</a>
                    @php($user =  Cas::user() )
                    @if(\App\Administrator::where('username',$user)->first())
                        <a class="page-scroll" href="/">Dash</a>
                    @endif
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>

<header>
    <div class="header-content">
        <div class="header-content-inner">
            <h1>SULF</h1>
            <hr>
            <!-- <p>Strathmore University Portal to help you recover your Lost and Found items!</p> -->
            <p>Enter Your Student/Staff ID Number</p>
            {!! Form::open(['action' => 'WelcomeController@postStudentLogin']) !!}
              <div class="form-inline">
                <div class="form-group-lg">
                    <input type="text" class="form-control" placeholder="Enter ID Number" name="suid" required="required" autofocus/>
                    <button  class="btn btn-primary btn-xl page-scroll" type="submit" name="approve">Continue!!</button>
                </div>
                @if ($errors->has('suid'))
                  <span class="help-block">
                    <strong>{{ $errors->first('suid') }}</strong>
                  </span>
                @endif
              </div>
            {!! Form::close() !!}
        </div>
    </div>
</header>


@endsection
