@extends('AdminDash.main')
<link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/buttons/1.4.1/css/buttons.dataTables.min.css" rel="stylesheet">
@section('title')
Contributed Items
@endsection()

@section('content')
      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3>
                    Contributed Items
                </h3>
            </div>


          </div>
          <div class="clearfix"></div>

        <div class="row">




          <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                      <div class="x_title">
                        <h2> </h2>
                        <ul class="nav navbar-right panel_toolbox">
                          <li><a href="#"><i class="fa fa-chevron-up"></i></a>
                          </li>
                          <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                              <li><a href="#">Settings 1</a>
                              </li>
                              <li><a href="#">Settings 2</a>
                              </li>
                            </ul>
                          </li>
                          <li><a href="#"><i class="fa fa-close"></i></a>
                          </li>
                        </ul>
                        <div class="clearfix"></div>
                      </div>
                      <div class="x_content">
                        <p class="text-muted font-13 m-b-30">

                        </p>
                        <table id="example" class="display" cellspacing="0" width="100%">
                          <thead>
                            <tr>
                              <th>Serial Number</th>
                              <th>Sub Category</th>
                              <th>Brand</th>
                              <th>Model</th>
                              <th>Primary Color</th>
                              <th>Secondary Color</th>
                              <th>Area</th>
                              <th>Registered</th>
                              <th>Registerer</th>
                              <th>Finder SUID</th>
                              <th>Finder Phone</th>
                              <th>Finder Email</th>
                            </tr>
                          </thead>
                          <tbody>
                            @foreach (\App\Item::where('status', 190)->get(); as $items)
                            <tr>
                              <td>{{$items->serialnumber}}</td>
                              <td>
                                {{
                                  $userval = DB::table('sub_categories')->where('id', $items->subcategory)->value('subcategory')
                                }}
                              </td>
                              <td>{{$items->brand}}</td>
                                <td>{{$items->model}}</td>
                              <td>
                                {{
                                  $userval = DB::table('colors')->where('id', $items->primarycolorid)->value('color')
                                }}
                              </td>
                              <td>
                                {{
                                  $userval = DB::table('colors')->where('id', $items->secondarycolorid)->value('color')
                                }}
                              </td>
                              <td>
                                  {{
                                    $userval = DB::table('areas')->where('id', $items->areaid)->value('area')
                                  }}
                              </td>
                              <td>{{$items->created_at}}</td>
                              <td>
                                {{
                                  $userval = DB::table('users')->where('id', $items->userid)->value('name')
                                }}</td>
                                <td>{{$items->findersuid}}
                                </td>
                                <td>{{$items->finderphonenumber}}
                                </td>
                                <td>{{$items->description}}
                                </td>

                            </tr>
                            @endforeach()
                          </tbody>
                        </table>

                      </div>
                    </div>
                  </div>

        </div>
        <br />
        <script type="text/javascript">
          $(document).ready(function() {
            $('#birthday').daterangepicker({
              singleDatePicker: true,
              calender_style: "picker_4"
            }, function(start, end, label) {
              console.log(start.toISOString(), end.toISOString(), label);
            });
          });
        </script>
        <script>
        $('#category').on('change',function (e) {
                  var category = e.target.value;
                  $.get('../sulf/ajax/category/'+category, function (data) {
                      $('#subcategory').empty();
                      $('#subcategory').append('<option value="">Select SubCategory</option>');
                      $('#subcategory').append('<option value="">---------------</option>');
                      $.each(data, function (data, subcategory) {
                          $('#subcategory').append('<option value="'+subcategory.id+'">'+subcategory.subcategory+'</option>')
                      })
                  });
                  console.log();
              });
        </script>
        <script>
        $('#subcategory').on('change',function (e) {
                  var subcategory = e.target.value;
                  $.get('../sulf/ajax/subcategory/'+subcategory, function (data) {
                      $('#brand').empty();
                      $('#brand').append('<option value="">Select Brand</option>');
                      $('#brand').append('<option value="">---------------</option>');
                      $.each(data, function (data, brand) {
                          $('#brand').append('<option value="'+brand.id+'">'+brand.brand+'</option>')
                      })
                  });
                  console.log();
              });
        </script>
        <script>
        $('#location').on('change',function (e) {
                  var location = e.target.value;
                  $.get('../sulf/ajax/location/'+location, function (data) {
                      $('#area').empty();
                      $('#area').append('<option value="">Select Areas</option>');
                      $('#area').append('<option value="">---------------</option>');
                      $.each(data, function (data, area) {
                          $('#area').append('<option value="'+area.id+'">'+area.area+'</option>')
                      })
                  });
                  console.log();
              });
        </script>
        <script>
        $(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ]
    } );
} );

        </script>

        <script src="//code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.4.1/js/dataTables.buttons.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
        <script src="//cdn.datatables.net/buttons/1.4.1/js/buttons.html5.min.js"></script>

        <!-- footer content -->

        @include('AdminDash.partials._footnote')
        <!-- /footer content -->
      </div>
      <!-- /page content -->

    @endsection()
