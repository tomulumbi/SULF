@extends('AdminDash.main')

@section('title')
Registered Brands
@endsection()

@section('content')
      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3>
                    Registered Brands
                </h3>
            </div>

            <div class="title_right">
              <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <span class="">
                          <a href="{{route('brand.create')}}" class="btn btn-success btn-block">Register Brands!</a>
                  </span>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>

        <div class="row">




          <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                      <div class="x_title">
                        <h2> </h2>
                        <ul class="nav navbar-right panel_toolbox">
                          <li><a href="#"><i class="fa fa-chevron-up"></i></a>
                          </li>
                          <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                          </li>
                          <li><a href="#"><i class="fa fa-close"></i></a>
                          </li>
                        </ul>
                        <div class="clearfix"></div>
                      </div>
                      <div class="x_content">
                        <p class="text-muted font-13 m-b-30">

                        </p>
                        <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                          <thead>
                            <tr>
                              <th>Brand</th>
                              <th>Sub-Category</th>
                              <th>Registerer</th>
                              <th>Registered</th>
                              <th>Update</th>
                              <th>Delete</th>
                            </tr>
                          </thead>
                          <tbody>
                            @foreach (\App\Brand::all(); as $brands)
                            <tr>
                              <td>{{$brands->brand}}</td>
                              <td>{{
                                $userval = DB::table('sub_categories')->where('id', $brands->subcategoryid)->value('subcategory')
                              }}</td>
                              <td>
                                {{
                                  $userval = DB::table('users')->where('id', $brands->userid)->value('name')
                                }}</td>
                              <td>{{$brands->created_at}}</td>
                              <td><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#{{$brands->id}}">Update</button></td>
                              <!-- Modal for Update -->
                              <div id="{{$brands->id}}" class="modal fade" role="dialog">
                                <div class="modal-dialog">

                                  <!-- Modal content-->
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                                      <h4 class="modal-title">Update Brand</h4>
                                    </div>
                                    <div class="modal-body">
                                      <h2>WARNING! ALTERING THIS WILL AFFECT ALL CLAIMS AND ITEMS REGISTERED UNDER THIS BRAND</h2>
                                      {{ Form::model($brands, array('route' => array('brand.update', encrypt($brands->id)), 'method' => 'PUT')) }}
                                      <div id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">
                                        <div class="row">
                                          <div class="col-md-8 col-md-offset-2">
                                            <div class="col-md-8 col-md-offset-2">
                                              <select name="category" id="category" class="form-control">

                                                  <option value="">Select Category...</option>
                                                  @foreach(\App\Category::all() as $categories)
                                                      <option value="{{($categories->id)}}">{{$categories->category}}</option>
                                                  @endforeach()
                                              </select>
                                          </div>
                                          <div class="form-group">

                                            <div class="col-md-8 col-md-offset-2">
                                                <select name="subcategory" id="subcategory" class="form-control">
                                                  @if(old('subcategory') != null)
                                                      <option value="{{old('subcategory')}}">{{\App\SubCategories::find(decrypt(old('subcategory')))->category}}</option>
                                                  @endif
                                                  <option value="">Select Sub Category...</option>
                                                </select>
                                            </div>
                                            <span class="help-block" id="subategoryerror">
                                              @if ($errors->has('subcategory')) {{$errors->first('subcategory')}} @endif
                                          </span>
                                          </div>
                                            <div class="form-group">

                                              <div class="col-md-8 col-md-offset-2">
                                                <input type="text" id="brand" name="brand" value="{{$brands->brand}}" placeholder="Brand" required="required" class="form-control col-md-7">
                                              </div>
                                              @if ($errors->has('brand'))
                                  						  <span class="help-block">
                                  								<strong>{{ $errors->first('brand') }}</strong>
                                  							</span>
                                  					  @endif
                                            </div>
                                          </div>
                                          </div>
                                        <br />
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                          <div class="col-md-6 col-sm-6 col-md-offset-5">
                                            <button type="submit" class="btn btn-success">Submit</button>
                                          </div>
                                        </div>

                                      </div>
                                      {!! Form::close() !!}
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                  </div>

                                </div>
                              </div>
                              <td><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete{{$brands->id}}">Delete</button></td>
                              <!-- Modal -->
                                <div id="delete{{$brands->id}}" class="modal fade" role="dialog">
                                  <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Modal Header</h4>
                                      </div>
                                      <div class="modal-body">
                                        <h2>WARNING! ALTERING THIS WILL AFFECT ALL CLAIMS AND ITEMS REGISTERED UNDER THIS SUBCATEGORY</h2>
                                        {!! Form::open([
                                            'method' => 'DELETE',
                                            'route' => ['brand.destroy', encrypt($brands->id)]
                                        ])  !!}
                                        {!! Form::token(); !!}
                                            {!! Form::submit('Delete?', ['class' => 'btn btn-danger']) !!}
                                        {!! Form::close() !!}
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                      </div>
                                    </div>

                                  </div>
                                </div>
                            </tr>
                            @endforeach()
                          </tbody>
                        </table>

                      </div>
                    </div>
                  </div>
        </div>
        <br />
        <script type="text/javascript">
          $(document).ready(function() {
            $('#birthday').daterangepicker({
              singleDatePicker: true,
              calender_style: "picker_4"
            }, function(start, end, label) {
              console.log(start.toISOString(), end.toISOString(), label);
            });
          });
        </script>
        <script>
        $('#category').on('change',function (e) {
                  var category = e.target.value;
                  $.get('../sulf/ajax/category/'+category, function (data) {
                      $('#subcategory').empty();
                      $('#subcategory').append('<option value="">Select SubCategory</option>');
                      $('#subcategory').append('<option value="">---------------</option>');
                      $.each(data, function (data, subcategory) {
                          $('#subcategory').append('<option value="'+subcategory.id+'">'+subcategory.subcategory+'</option>')
                      })
                  });
                  console.log();
              });
        </script>
        <script>
        $('#subcategory').on('change',function (e) {
                  var subcategory = e.target.value;
                  $.get('../sulf/ajax/subcategory/'+subcategory, function (data) {
                      $('#brand').empty();
                      $('#brand').append('<option value="">Select Brand</option>');
                      $('#brand').append('<option value="">---------------</option>');
                      $.each(data, function (data, brand) {
                          $('#brand').append('<option value="'+brand.id+'">'+brand.brand+'</option>')
                      })
                  });
                  console.log();
              });
        </script>
        <script>
        $('#location').on('change',function (e) {
                  var location = e.target.value;
                  $.get('/sulf/ajax/location/'+location, function (data) {
                      $('#area').empty();
                      $('#area').append('<option value="">Select Areas</option>');
                      $('#area').append('<option value="">---------------</option>');
                      $.each(data, function (data, area) {
                          $('#area').append('<option value="'+area.id+'">'+area.area+'</option>')
                      })
                  });
                  console.log();
              });
        </script>
        <!-- footer content -->
        @include('AdminDash.partials._footnote')
        <!-- /footer content -->
      </div>
      <!-- /page content -->
    @endsection()
