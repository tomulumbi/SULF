@if(Session::has('userregistered'))
<div class="col-md-6 col-md-offset-3">
  <div class="alert alert-warning alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Warning!</strong>{{Session::get('userregistered')}}
  </div>
</div>
@endif

@if(Session::has('registered'))
<div class="col-md-6 col-md-offset-3">
  <div class="alert alert-success alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong>{{Session::get('registered')}}
  </div>
</div>
@endif
