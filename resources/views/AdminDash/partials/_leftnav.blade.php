<div class="col-md-3 left_col">
        <div class="left_col scroll-view">

          <div class="navbar nav_title" style="border: 0;">
            <a href="#" class="site_title"><i class="fa fa-search"></i> <span>SULF</span></a>
          </div>
          <div class="clearfix"></div>

          <!-- menu prile quick info -->
          <div class="profile">
            <div class="profile_pic">
              <img src="{{asset('/Gentella/images/img.jpg')}}" alt="..." class="img-circle profile_img">
            </div>
            <div class="profile_info">
              <span>Welcome,</span>
              <h2><h2>{{ $value }}</h2></h2>
            </div>
          </div>
          <!-- /menu prile quick info -->

          <br />


          <!-- sidebar menu -->
          <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

            <div class="menu_section">
              <h3>General</h3>
              <ul class="nav side-menu">
                <li><a><i class="fa fa-home"></i> Home <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu" style="display: none">
                  <li><a href="{{url('admindash')}}">Dashboard</a>
                    </li>
                    <li><a href="{{url('category')}}">Categories</a>
                    </li>
                    <li><a href="{{url('subcategory')}}">Subcategories</a>
                    </li>
                    <!-- <li><a href="{{url('brand')}}">Brands</a>
                    </li> -->
                    <li><a href="{{url('color')}}">Colors</a>
                    </li>
                    <li><a href="{{url('location')}}">Locations</a>
                    </li>
                    <li><a href="{{url('area')}}">Areas</a>
                    </li>
                  </ul>
                </li>
                <li><a><i class="fa fa-edit"></i> Items <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu" style="display: none">
                    <li><a href="{{url('item')}}">Registered</a>
                    </li>
                  </ul>
                </li>
                <li><a><i class="fa fa-desktop"></i> Claims <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu" style="display: none">
                    <li><a href="{{url('reclaimeditems')}}"> Approved </a>
                    </li>
                    <li><a href="{{url('pendingclaims')}}"> Pending </span></a>
                    </li>
                    <li><a href="{{url('deletedclaims')}}"> Deleted </span></a>
                    </li>
                    <li><a href="{{url('contributed')}}"> Contributed </span></a>
                    </li>
                  </ul>
                </li>
                <li><a><i class="fa fa-desktop"></i> Administrators <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu" style="display: none">
                    <li><a href="{{url('admins/create')}}"> Register</a>
                    </li>
                    <li><a href="{{url('admins')}}"> Registered</a>
                    </li>
                  </ul>
                </li>
              </ul>
            </div>
          </div>
          <!-- /sidebar menu -->
        </div>
      </div>
