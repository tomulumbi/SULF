@extends('AdminDash.main')

@section('title')
Brand Registration
@endsection()

@section('content')

      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3>Brand Registration Form</h3>
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">

                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <br />
                  {!!  Form::open(['route' => 'brand.store']) !!}
                  <div id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">
                  <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                      <div class="col-md-8 col-md-offset-2">
                        <select name="category" id="category" class="form-control">

                            <option value="">Select Category...</option>
                            @foreach(\App\Category::all() as $categories)
                                <option value="{{($categories->id)}}">{{$categories->category}}</option>
                            @endforeach()
                        </select>
                    </div>
                    <div class="form-group">

                      <div class="col-md-8 col-md-offset-2">
                          <select name="subcategory" id="subcategory" class="form-control">
                            @if(old('subcategory') != null)
                                <option value="{{old('subcategory')}}">{{\App\SubCategories::find(decrypt(old('subcategory')))->category}}</option>
                            @endif
                            <option value="">Select Sub Category...</option>
                          </select>
                      </div>
                      <span class="help-block" id="subategoryerror">
                        @if ($errors->has('subcategory')) {{$errors->first('subcategory')}} @endif
                    </span>
                    </div>
                      <div class="form-group">

                        <div class="col-md-8 col-md-offset-2">
                          <input type="text" id="brand" name="brand" placeholder="Brand" required="required" class="form-control col-md-7">
                        </div>
                        @if ($errors->has('brand'))
            						  <span class="help-block">
            								<strong>{{ $errors->first('brand') }}</strong>
            							</span>
            					  @endif
                      </div>
                    </div>
                    </div>
                    <br />
                    <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-md-offset-5">
                        <button type="submit" class="btn btn-primary">Cancel</button>
                        <button type="submit" class="btn btn-success">Submit</button>
                      </div>
                    </div>

                  </div>
                  {!! Form::close() !!}
                </div>
              </div>
            </div>
          </div>

          <script type="text/javascript">
            $(document).ready(function() {
              $('#birthday').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_4"
              }, function(start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
              });
            });
          </script>
          <script>
          $('#category').on('change',function (e) {
                    var category = e.target.value;
                    $.get('../sulf/ajax/category/'+category, function (data) {
                        $('#subcategory').empty();
                        $('#subcategory').append('<option value="">Select SubCategory</option>');
                        $('#subcategory').append('<option value="">---------------</option>');
                        $.each(data, function (data, subcategory) {
                            $('#subcategory').append('<option value="'+subcategory.id+'">'+subcategory.subcategory+'</option>')
                        })
                    });
                    console.log();
                });
          </script>
          <script>
          $('#subcategory').on('change',function (e) {
                    var subcategory = e.target.value;
                    $.get('../sulf/ajax/subcategory/'+subcategory, function (data) {
                        $('#brand').empty();
                        $('#brand').append('<option value="">Select Brand</option>');
                        $('#brand').append('<option value="">---------------</option>');
                        $.each(data, function (data, brand) {
                            $('#brand').append('<option value="'+brand.id+'">'+brand.brand+'</option>')
                        })
                    });
                    console.log();
                });
          </script>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        @include('AdminDash.partials._footnote')
        <!-- /footer content -->

      </div>

    @endsection()
