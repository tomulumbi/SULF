@extends('AdminDash.main')

@section('title')
Registered Sub-Categories
@endsection()

@section('content')
      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3>
                    Registered Sub-Categories
                </h3>
            </div>

            <div class="title_right">
              <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <span class="">
                          <a href="{{route('subcategory.create')}}" class="btn btn-success btn-block">Register Sub-Category!</a>
                  </span>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>

        <div class="row">




          <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                      <div class="x_title">
                        <h2> </h2>
                        <ul class="nav navbar-right panel_toolbox">
                          <li><a href="#"><i class="fa fa-chevron-up"></i></a>
                          </li>
                          <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                              <li><a href="#">Settings 1</a>
                              </li>
                              <li><a href="#">Settings 2</a>
                              </li>
                            </ul>
                          </li>
                          <li><a href="#"><i class="fa fa-close"></i></a>
                          </li>
                        </ul>
                        <div class="clearfix"></div>
                      </div>
                      <div class="x_content">
                        <p class="text-muted font-13 m-b-30">

                        </p>
                        <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                          <thead>
                            <tr>
                              <th>Sub-Category</th>
                              <th>Category</th>
                              <th>Registerer</th>
                              <th>Registered</th>
                              <th>Update</th>
                              <th>Delete</th>
                            </tr>
                          </thead>
                          <tbody>
                            @foreach (\App\SubCategories::all(); as $category)
                            <tr>
                              <td>{{$category->subcategory}}</td>
                              <td>{{
                                $userval = DB::table('categories')->where('id', $category->categoryid)->value('category')
                              }}</td>
                              <td>
                                {{
                                  $userval = DB::table('users')->where('id', $category->userid)->value('name')
                                }}</td>
                              <td>{{$category->created_at}}</td>
                              <td><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#{{$category->id}}">Update</button></td>
                              <!-- Modal for Update -->
                              <div id="{{$category->id}}" class="modal fade" role="dialog">
                                <div class="modal-dialog">

                                  <!-- Modal content-->
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                                      <h4 class="modal-title">Update SubCategory</h4>
                                    </div>
                                    <div class="modal-body">
                                      <h2>WARNING! ALTERING THIS WILL AFFECT ALL BRANDS, CLAIMS AND ITEMS REGISTERED UNDER THIS SUBCATEGORY</h2>
                                      <br>
                                      <br>
                                      {{ Form::model($category, array('route' => array('subcategory.update', encrypt($category->id)), 'method' => 'PUT')) }}
                                      <div id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">
                                        <div class="row">
                                          <div class="">
                                            <div class="">
                                              <select name="category" id="" class="form-control">
                                                  @if(old('category') != null)
                                                      <option value="{{old('category')}}">{{\App\Category::find(decrypt(old('category')))->name}}</option>
                                                  @endif
                                                  <option value="">Select Category...</option>
                                                  @foreach(\App\Category::all() as $categories)
                                                      <option value="{{encrypt($categories->id)}}">{{$categories->category}}</option>
                                                  @endforeach()
                                              </select>
                                          </div>
                                            <div class="form-group">
                                              <div class="">
                                                <input type="text" id="subcategory" placeholder="Sub Category" value="{{$category->subcategory}}" name="subcategory" required="required" class="form-control col-md-7">
                                              </div>
                                              @if ($errors->has('subcategory'))
                                  						  <span class="help-block">
                                  								<strong>{{ $errors->first('subcategory') }}</strong>
                                  							</span>
                                  					  @endif
                                            </div>
                                          </div>
                                          </div>
                                        <br />
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                          <div class="col-md-6 col-sm-6 col-md-offset-5">
                                            <button type="submit" class="btn btn-success">Submit</button>
                                          </div>
                                        </div>

                                      </div>
                                      {!! Form::close() !!}
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                  </div>

                                </div>
                              </div>
                              <td><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete{{$category->id}}">Delete</button></td>
                              <!-- Modal -->
                                <div id="delete{{$category->id}}" class="modal fade" role="dialog">
                                  <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Modal Header</h4>
                                      </div>
                                      <div class="modal-body">
                                        <h2>WARNING! ALTERING THIS WILL AFFECT ALL BRANDS, CLAIMS AND ITEMS REGISTERED UNDER THIS SUBCATEGORY</h2>
                                        {!! Form::open([
                                            'method' => 'DELETE',
                                            'route' => ['subcategory.destroy', encrypt($category->id)]
                                        ])  !!}
                                        {!! Form::token(); !!}
                                            {!! Form::submit('Delete?', ['class' => 'btn btn-danger']) !!}
                                        {!! Form::close() !!}
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                      </div>
                                    </div>

                                  </div>
                                </div>
                            </tr>
                            @endforeach()
                          </tbody>
                        </table>

                      </div>
                    </div>
                  </div>

        </div>
        <br />


        <!-- footer content -->
        <script type="text/javascript">
          $(document).ready(function() {
            $('#birthday').daterangepicker({
              singleDatePicker: true,
              calender_style: "picker_4"
            }, function(start, end, label) {
              console.log(start.toISOString(), end.toISOString(), label);
            });
          });
        </script>
        <script>
        $('#category').on('change',function (e) {
                  var category = e.target.value;
                  $.get('../sulf/ajax/category/'+category, function (data) {
                      $('#subcategory').empty();
                      $('#subcategory').append('<option value="">Select SubCategory</option>');
                      $('#subcategory').append('<option value="">---------------</option>');
                      $.each(data, function (data, subcategory) {
                          $('#subcategory').append('<option value="'+subcategory.id+'">'+subcategory.subcategory+'</option>')
                      })
                  });
                  console.log();
              });
        </script>
        <script>
        $('#subcategory').on('change',function (e) {
                  var subcategory = e.target.value;
                  $.get('../sulf/ajax/subcategory/'+subcategory, function (data) {
                      $('#brand').empty();
                      $('#brand').append('<option value="">Select Brand</option>');
                      $('#brand').append('<option value="">---------------</option>');
                      $.each(data, function (data, brand) {
                          $('#brand').append('<option value="'+brand.id+'">'+brand.brand+'</option>')
                      })
                  });
                  console.log();
              });
        </script>
        <script>
        $('#location').on('change',function (e) {
                  var location = e.target.value;
                  $.get('/sulf/ajax/location/'+location, function (data) {
                      $('#area').empty();
                      $('#area').append('<option value="">Select Areas</option>');
                      $('#area').append('<option value="">---------------</option>');
                      $.each(data, function (data, area) {
                          $('#area').append('<option value="'+area.id+'">'+area.area+'</option>')
                      })
                  });
                  console.log();
              });
        </script>
        @include('AdminDash.partials._footnote')
        <!-- /footer content -->
      </div>
      <!-- /page content -->

    @endsection()
