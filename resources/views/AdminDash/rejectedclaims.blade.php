@extends('AdminDash.main')

@section('title')
Rejected Claims
@endsection()

@section('content')
      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3>
                    Rejected Claims
                </h3>
            </div>

            <div class="title_right">
              <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">

              </div>
            </div>
          </div>
          <div class="clearfix"></div>

        <div class="row">




          <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                      <div class="x_title">
                        <h2> </h2>
                        <ul class="nav navbar-right panel_toolbox">
                          <li><a href="#"><i class="fa fa-chevron-up"></i></a>
                          </li>
                          <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                              <li><a href="#">Settings 1</a>
                              </li>
                              <li><a href="#">Settings 2</a>
                              </li>
                            </ul>
                          </li>
                          <li><a href="#"><i class="fa fa-close"></i></a>
                          </li>
                        </ul>
                        <div class="clearfix"></div>
                      </div>
                      <div class="x_content">
                        <p class="text-muted font-13 m-b-30">

                        </p>
                        <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                          <thead>
                            <tr>
                              <th>Serial Number</th>
                              <th>Brand</th>
                              <th>Model</th>
                              <th>Primary Color</th>
                              <th>Secondary Color</th>
                              <th>Location</th>
                              <th>Area</th>
                              <th>Registered</th>
                              <th>Claimer</th>
                              <th>Finder</th>
                              <th>Contribute</th>
                            </tr>
                          </thead>
                          <tbody>
                            @foreach (\App\Claim::where('status', 55)->get(); as $claims)
                            <tr>
                              <td>{{$claims->serialnumber}}</td>
                              <td>
                                {{
                                  $userval = DB::table('brands')->where('id', $claims->brandid)->value('brand')
                                }}
                              </td>
                                <td>{{$claims->model}}</td>
                              <td>
                                {{
                                  $userval = DB::table('colors')->where('id', $claims->primarycolorid)->value('color')
                                }}
                              </td>
                              <td>
                                {{
                                  $userval = DB::table('colors')->where('id', $claims->secondarycolorid)->value('color')
                                }}
                              </td>
                              <td>
                                  {{
                                    $userval = DB::table('locations')
                                    ->join('areas', 'locations.id', '=', 'areas.locationid')
                                    ->value('locations.location')
                                  }}
                              </td>
                              <td>
                                  {{
                                    $userval = DB::table('areas')->where('id', $claims->areaid)->value('area')
                                  }}
                              </td>
                              <td>{{$claims->created_at}}</td>
                              <td>
                                {{
                                  $userval = DB::table('students')->where('id', $claims->studentid)->value('suid')
                                }}</td>

                              <!-- Accept Modal -->
                  <div class="modal fade" id="{{$claims->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                          <h4 class="modal-title" id="myModalLabel">More Detail Modal</h4>
                        </div>
                        <div class="modal-body">
                          <form role="form">
                            <div class="box-body">
                              <div class="form-group">
                                <label for="exampleInputPassword1">Finder SUID</label>
                                <input type="text" readonly="readonly" value="{{$claims->claimersuid}}" class="form-control" id="_administrator" placeholder="">
                              </div>
                              <div class="form-group">
                                <label for="exampleInputPassword1">Finder Email</label>
                                <input type="text" readonly="readonly" value="{{$claims->claimeremail}}" class="form-control" id="_administrator" placeholder="">
                              </div>
                              <div class="form-group">
                                <label for="exampleInputPassword1">Finder Phone Number</label>
                                <input type="text" readonly="readonly" value="{{$claims->claimerphonenumber}}" class="form-control" id="_administrator" placeholder="">
                              </div>
                              <div class="form-group">
                                <label for="exampleInputEmail1">Reason</label>
                                <textarea class="form-control" id="_reason" readonly="readonly" rows="3" placeholder="{{$claims->description}}"></textarea>
                              </div>

                            </div>
                            <!-- /.box-body -->

                            <div class="box-footer">
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                  <td><span class="btn btn-success btn-block" data-toggle="modal" data-target="#{{$claims->id}}">More</span></td>
                  <td><a class="btn btn-success btn-block">Contribute</a></td>
                            </tr>
                            @endforeach()
                          </tbody>
                        </table>

                      </div>
                    </div>
                  </div>

        </div>
        <br />


        <!-- footer content -->

        @include('AdminDash.partials._footnote')
        <!-- /footer content -->
      </div>
      <!-- /page content -->

    @endsection()
